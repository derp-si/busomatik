package com.VegaSolutions.lpptransit.ui.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceFragmentCompat;

import com.VegaSolutions.lpptransit.R;

public class SettingsActivity extends AppCompatActivity  {

    public static final int SETTINGS_UPDATE = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        SharedPreferences sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
//        sharedPreferences.registerOnSharedPreferenceChangeListener(this);


        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings_layout, new MySettingsFragment())
                .commit();
        setTitle(R.string.settings);
    }


    public static void updateDarkMode(Context ctx, SharedPreferences sharedPreferences, String darkModeString) {
        String[] darkModeValues = ctx.getResources().getStringArray(R.array.pref_themes_values);
        String val = sharedPreferences.getString(darkModeString, darkModeValues[0]);
        assert val != null;
        if (val.equals(darkModeValues[0])) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        } else if (val.equals(darkModeValues[1])) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else if (val.equals(darkModeValues[2])) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }


    public static class MySettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            getPreferenceManager().setSharedPreferencesName("settings");
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            String darkModeString = getString(R.string.pref_theme_key);
            if (key.equals(darkModeString)) {
                SettingsActivity.updateDarkMode(getContext(), sharedPreferences, darkModeString);
            }
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }


    }

}