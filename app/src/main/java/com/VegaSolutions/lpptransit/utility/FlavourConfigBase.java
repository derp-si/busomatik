package com.VegaSolutions.lpptransit.utility;

import com.VegaSolutions.lpptransit.lppapi.responseobjects.Station;
import com.google.android.gms.maps.model.LatLng;

public interface FlavourConfigBase {
    public static FlavourConfig instance = new FlavourConfig();

    default LatLng getMapCenter() {
        return new LatLng(46.056319, 14.505381);
    }

    default boolean isStationCenter(Station s) {
        return false;
    }
}
